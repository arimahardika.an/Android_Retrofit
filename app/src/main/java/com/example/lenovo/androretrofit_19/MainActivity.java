package com.example.lenovo.androretrofit_19;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lenovo.androretrofit_19.api.model.GithubRepo;
import com.example.lenovo.androretrofit_19.api.service.GithubClient;
import com.example.lenovo.androretrofit_19.ui.adapter.GithubRepoAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity {

    private ListView listView;
    private EditText githubUsername;
    private Button findButton;
    View emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.lv_repo);
        githubUsername = findViewById(R.id.et_githubUsername);
        githubUsername.requestFocus();

        findButton = findViewById(R.id.bt_find);

        findButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(githubUsername.getText().toString().matches(" ")){
                    Toast.makeText(MainActivity.this, "Edit text is empty", Toast.LENGTH_SHORT).show();
                }else{
                    FindGithubUsername(githubUsername.getText().toString());
                }
                //Toast.makeText(MainActivity.this, githubUsername.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void FindGithubUsername(String username) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        GithubClient client = retrofit.create(GithubClient.class);
        Call<List<GithubRepo>> call = client.reposForUser(username);

        call.enqueue(new Callback<List<GithubRepo>>() {
            @Override
            public void onResponse(Call<List<GithubRepo>> call, Response<List<GithubRepo>> response) {
                if(response.body() == null){
                    Toast.makeText(MainActivity.this, "Username not found", Toast.LENGTH_SHORT).show();
                }else if(response.body() != null){
                    List<GithubRepo> repos = response.body();
                    listView.setAdapter(new GithubRepoAdapter(MainActivity.this, repos));
                }
            }

            @Override
            public void onFailure(Call<List<GithubRepo>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error . . .", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
